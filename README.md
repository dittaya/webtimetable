# WebTimetable

Use Google Sheet as a database for timetable management.

# Current status
- render a timetable for each instructor / room
- update a time slot of each insturctor / room

# TODO
- create a new time slot
- temporary area for moved slots
- delete a slot
- view for all instructors / rooms
- move between instructor / room via multitable view
- editable text in a slot