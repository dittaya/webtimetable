/*jslint white: true, browser: true, undef: true, nomen: true, eqeqeq: true, plusplus: false, bitwise: true, regexp: true, strict: true, newcap: true, immed: true, maxerr: 14 */
/*global window: false, REDIPS: true, $: true */

/* enable strict mode */
"use strict";

var redips = {};
var redipsTable = REDIPS.table;;
var redipsDrag = REDIPS.drag;

var updateList = [];

// REDIPS.drag initialization
redips.init = function () {
	// define reference to the REDIPS.table object
	var msg = document.getElementById('message');
    var buttonL = document.getElementById("loadBtn");
    var buttonC = document.getElementById("clearBtn");
    var buttonUR = document.getElementById("updateBtnR");
    var buttonUT = document.getElementById("updateBtnT");
    var buttonM = document.getElementById("mergeBtn");
    var buttonSa = document.getElementById("splitaBtn");
    var buttonA = document.getElementById("addColBtn");
    var buttonChange = document.getElementById("changeType");


    var mainTable = document.getElementById('mainTable');

   	// activate onmousedown event listener on cells within table with id="mainTable"
	redipsTable.onmousedown('mainTable', true);
    // show cellIndex (it is nice for debugging)
    redipsTable.cell_index(true);
    // define background color for marked cell
    redipsTable.color.cell = '#9BB3DA';
    // REDIPS.drag initialization
    redipsDrag.init();
    // set hover color
    //rd.hover.colorTd = '#9BB3DA';

    // ignore the first row of the main table
    for (var r = 0; r < mainTable.rows.length; r++) {
        redipsTable.cell_ignore(mainTable.rows[r].cells[0]);
    }

    // ignore the first column of the main table
    for (var c = 0; c < mainTable.rows[0].cells.length; c++) {
        redipsTable.cell_ignore(mainTable.rows[0].cells[c]);
    }

    // set header
    var timeSlot;
    for (var c = 1; c < 10; c++) {
        timeSlot = mainTable.rows[0].cells[2*c-1];
        timeSlot.innerHTML = (c+7).toString() + ':00-' + (c+8).toString() + ':00';
    }
    // merge header
    for (var c = 1; c < 10; c++) {
        redipsTable.mark(true, 'mainTable', 0, 2*c-1);
        redipsTable.mark(true, 'mainTable', 0, 2*c);
        redipsTable.merge('h', true);
    }
    timeSlot = mainTable.rows[0].cells[10];
    timeSlot.innerHTML = '18.00-21.00';

    redipsDrag.event.dropped = function () {
        var oldNode = redipsDrag.td.source;
        var newNode = redipsDrag.td.current;

        var box = redipsDrag.obj;
        var rowID = box.id;

        console.log(rowID, 'Move from '+decodeSlot(oldNode)+ " to " + decodeSlot(newNode));

        var colID = newNode.id.split('-');
        var day = encodeDay(Number(colID[0]));
        var time = encodeTime(Number(colID[1]), newNode.colSpan);

        // data in dataMap is starting from row 0
        // data in Google sheet is starting from row 2
        var entry = dataMap[rowID-2];
        entry['Meeting Day'] = day;
        entry['Meeting Time'] = time;
        entry['Col Index'] = newNode.id;
        console.log(dataMap[rowID-2]);
        // update(entry);

		// msg.innerHTML = 'Dropped';
		// redipsTable.cell_index(true);
		// var check = redips.checkTable();
		// var newParent = redipsDrag.td.current;
		// var oldParent = redipsDrag.td.source;
		// if ( (newParent.childElementCount > 1) || (oldParent.childElementCount > 1) ) {
  //   		if (newParent.childElementCount > 1) { 
  //               newParent.style.backgroundColor = "#FF3737";
  //   		} else {
  //               newParent.style.backgroundColor = "#eee";
  //           }
  //   		if (oldParent.childElementCount > 1) { 
  //   		    redipsDrag.td.source.style.backgroundColor = "#FF3737";
  //   		} else {
  //               oldParent.style.backgroundColor = "#eee";
  //           }

  //   		redipsTable.onmousedown('mainTable', false);
		// } else {
  //   		newParent.style.backgroundColor = "#eee";
  //   		oldParent.style.backgroundColor = "#eee";
  //   		buttonL.disabled =  check;
  //   		buttonC.disabled =  check;
  //   		buttonUR.disabled =  check;
  //   		buttonUT.disabled =  check;
  //   		buttonM.disabled =  check;
  //   		buttonSa.disabled =  check;
  //           buttonChange.disabled = check;
  //   		buttonA.disabled = check;
  //   		redipsTable.onmousedown('mainTable', true);
  //       }

  //       buttonL.disabled =  check;
  //       buttonC.disabled =  check;
  //       buttonUR.disabled =  check;
  //       buttonUT.disabled =  check;
  //       buttonM.disabled =  check;
  //       buttonSa.disabled =  check;
  //       buttonA.disabled = check;
  //       buttonChange.disabled = check;
	};
	
    redipsDrag.event.changed = function () {
		// get target and source position (method returns positions as array)
		var pos = redipsDrag.getPosition();
		var children = redipsDrag.obj;
		var parent = children.parentElement;
		// display current row and current cell
		msg.innerHTML = 'Changed: ' + pos[1] + ' ' + pos[2]  +"from : "+ pos[4] + ' ' + pos[5];

        // console.log("Row "+children.id+" is moved to "+pos[1]+" "+pos[2]);
	};

};
//=============================================================================
redips.merge = function () {
    // first merge cells horizontally and leave cells marked
    redipsTable.merge('h',true);
    // table should be initialized after merging
    redipsTable.cell_index(true);
    redipsDrag.initTables();
};

redips.checkTable = function () {
    var mainTable = document.getElementById('mainTable');
    var moveTable = document.getElementById('moveTable');
    var conflictTable = document.getElementById('errorTable');

    // check for changes in the main table
    var rowSize = mainTable.rows.length;
    var colSize;
    for (var r = 1; r < rowSize; r++) {
        colSize = mainTable.rows[r].childElementCount;
        for (var c = 1; c < colSize; c++) {

            // deselect cells
            if (mainTable.rows[r].cells[c].redips.selected) {
                redipsTable.mark(false,'mainTable', r, c);
        	}

            // have some changes
        	if (mainTable.rows[r].cells[c].childElementCount > 1) {
        	    return true;
            }
        }
    }
    
    // check for changes in the moving table
    colSize = moveTable.rows[0].childElementCount;
    for (var c = 0; c < colSize; c++) {
    	if (mainTable.rows[0].cells[c].childElementCount > 1) {
    	    return true;
        }
    }

    // check for changes in the conflict table
    colSize = conflictTable.rows[0].childElementCount;
    for (var c = 0; c < colSize; c++) {
    	if (conflictTable.rows[0].cells[c].childElementCount > 1) {
    	    return true;
        }
    }

    // no change
    return false;
};

function decodeSlot(tdNode) {
    var colID = tdNode.id.split('-');
    var day = encodeDay(Number(colID[0]));
    var time = encodeTime(Number(colID[1]), tdNode.colSpan);
    return day+' '+time;
}

function getCellID(r, c) {
    return r.toString()+'-'+c.toString();
}

// method splits table cells if colspan/rowspan is greater then 1
// mode is 'h' or 'v' (cells should be marked before)
redips.splitAll = function (mode) {
    var mainTable = document.getElementById('mainTable');

    var selected = [];
    var rowSize = mainTable.rows.length;
    for (var r = 1; r < rowSize; r++) {
        var colSize = mainTable.rows[r].childElementCount;
        for (var c = 1; c < colSize; c++){
            if (mainTable.rows[r].cells[c].redips.selected) {
                selected.push(mainTable.rows[r].cells[c].id);
            }
        }
    }
    for (var sIndex in selected) {
        var cellRef = selected[sIndex];

        /* these lines are necessary. I don't understand why. */
        var cellIndex = cellRef.split('-');
        var r = cellIndex[0];
        var c = cellIndex[1];
        /* end of 'seem-to-be useless' lines */

        var nMerged = mainTable.rows[r].cells[cellRef].colSpan;
        for (var m = 0; m < nMerged; m++) {
            redips.split(mode);
            redipsTable.mark(true,cellRef);
        }
        redipsTable.mark(false,cellRef);
    }
    console.log('Split all selected cells');
};

redips.splitEmpty = function (mode) {
    console.log('Clear empty table');
    var rowSize = mainTable.rows.length;
    for (var r = 1; r < rowSize; r++) {
        var colSize = mainTable.rows[r].childElementCount;
        for (var c = 1; c < colSize; c++) {
            if (mainTable.rows[r].cells[c].redips.selected){
                redipsTable.mark(false,'mainTable', r, c);
            }

            // unmerge a cell
        	if ( (mainTable.rows[r].cells[c].childElementCount == 0)
                && (mainTable.rows[r].cells[c].colSpan > 1) ) {
                var emptyindex = mainTable.rows[r].cells[c].id.slice(2);
                redipsTable.mark(true,'mainTable', r, emptyindex );
                var nMerged = mainTable.rows[r].cells[c].colSpan;
                for (var m = 0; m < nMerged; m++){
                    redips.split();
                    redipsTable.mark(true,'mainTable', r, emptyindex);
                }
                redipsTable.mark(false,'mainTable', r, emptyindex);
            }
            // re-count the column size after split
            colSize = tbl.rows[r].childElementCount;
        }
    }
};

redips.split = function (mode) {
    redipsTable.split('h');
    // table should be initialized after merging
    redipsDrag.initTables();
};

redips.column = function (type) {
    var moveTable = document.getElementById('moveTable');
    redipsDrag.column(moveTable, "insert");
    //REDIPS.table.onmousedown(mtb, false);
};

redips.columnE = function (type) {
    var conflictTable = document.getElementById('errorTable');
    redipsTable.column(conflictTable, "insert");
    //REDIPS.table.onmousedown(etb, false);
};

redips.clearTable = function () {
    console.log("Clear Table");
    redipsDrag.clearTable('mainTable');
    redipsDrag.clearTable('moveTable');
    redipsDrag.clearTable('errorTable');

    var mainTable = document.getElementById('mainTable');
    var moveTable = document.getElementById('moveTable');
    var conflictTable = document.getElementById('errorTable');

    // clear all merged cells in the main table
    var rowSize = mainTable.rows.length;
    for (var r = 1; r < rowSize; r++) {
    	var colSize = mainTable.rows[r].childElementCount;
    	for (var c = 1; c < colSize; c++) {
            // unmark all cells
    		if (mainTable.rows[r].cells[c].redips.selected) {
                redipsTable.mark(false,'mainTable', r, c);
    		}
            // in the case of merged cells
    		if (mainTable.rows[r].cells[c].colSpan >= 1) {
    		    redipsTable.mark(true,'mainTable', r, c);
    		    var nMerged = mainTable.rows[r].cells[c].colSpan;
                for(var m = 0; m < nMerged ; m++){
                    redips.split();
                    redipsTable.mark(true,'mainTable', r, c);
                }
                redipsTable.mark(false,'mainTable', r, c);
            }
            // re-count the column size after split
            colSize = mainTable.rows[r].childElementCount;
        }
    }
        
    // clear the move table
    rowSize = moveTable.rows[0].childElementCount;
	for (r = 1; r < rowSize; r++) {
    	redipsTable.column(moveTable, "");
    }

    // clear the conflict table
    rowSize = conflictTable.rows[0].childElementCount;
	for (r = 1; r < rowSize; r++) {
		redipsTable.column(conflictTable, "");
    }
};

var headers;
var dataMap = [];
var roomList = [];
var instructorList = [];
var sheetID;
var lastRow;

function loadData() {
    dataMap = [];
    // get sheetID
    gapi.client.sheets.spreadsheets.get({
      spreadsheetId: SPREADSHEET_ID,
      range: [SHEET_NAME+'!'+'A:L'],
      includeGridData: false,
    }).then(function(response) {
      var sheets = response.result;
      sheetID = response.result.sheets[0].properties.sheetId;
    }, function(response) {
      console.log('Error: ' + response.result.error.message);
    });

    // read data
    gapi.client.sheets.spreadsheets.values.get({
      spreadsheetId: SPREADSHEET_ID,
      range: SHEET_NAME+'!'+'A:L',
    }).then(function(response) {
      var result = response.result;
      lastRow = result.values.length;
      console.log("Loading "+lastRow+" rows");
      headers = result.values[0];
      headers[10] = 'Midterm Exam Date';
      headers[11] = 'Final Exam Date';

      var roomSet = new Set();
      var instructorSet = new Set();

      for (var i = 1; i < result.values.length; i++) {
        var entry = result.values[i];
        var mapEntry = {};
        for (var h = 0; h < headers.length; h++) {
          if (h < entry.length && entry[h].length > 0) {
            mapEntry[headers[h]] = entry[h];  
          }
          else {
            mapEntry[headers[h]] = "";
          }
        }
        if (mapEntry['Room'].length > 0) {
          roomSet.add(mapEntry['Room']);
        }
        mapEntry['Meeting Day'] = mapEntry['Meeting Day'].split(' ');
        mapEntry['Instructor'] = mapEntry['Instructor'].trim().split(',')
        for (var index in mapEntry['Instructor']) {
          if (mapEntry['Instructor'][index].length > 0) {
           instructorSet.add(mapEntry['Instructor'][index]);
          }
        }
        // row index in Google Sheets starts with 1
        mapEntry['Row Index'] = (i+1).toString();
        dataMap.push(mapEntry);
      }
      console.log('Loading Done');

      var select = document.getElementById("byRoom");
      select.innerHTML = "";
      roomList = Array.from(roomSet);
      roomList.sort()
      for (var index in roomList) {
        var opt = document.createElement("option");
        var item = roomList[index];
        opt.value = item;
        opt.textContent = item;
        select.appendChild(opt);
      }
      console.log('Room option Done');

      select = document.getElementById("byInstructor");
      instructorList = Array.from(instructorSet);
      instructorList.sort()
      for (var index in instructorList) {
        var opt = document.createElement("option");
        item = instructorList[index];
        opt.value = item;
        opt.textContent = item;
        select.appendChild(opt);
      }
      console.log('Instructor option Done');

    }, function(response) {
      console.log('Error: ' + response.result.error.message);
    });
};

function decodeDay(day) {
    if (day == 'MO') return 1;
    else if (day == 'TU') return 2;
    else if (day == 'WE') return 3;
    else if (day == 'TH') return 4;
    else if (day == 'FR') return 5;
    else return 6;
};

function decodeTime(timeslot) {
    var slots = []
    if (timeslot.length > 0) {
      var time = timeslot.split('-');
      var startTime = time[0].split(':');
      var endTime = time[1].split(':');

      var startSlot = (Number(startTime[0]) - 8) * 2 + Number(startTime[1]) / 30 + 1;
      if (startSlot >= 19) {
        startSlot = 19;
      }
      var endSlot = (Number(endTime[0]) - 8) * 2 + Number(endTime[1]) / 30 + 1;
      if (endSlot >= 20) {
        endSlot = 20;
      }

      slots = [startSlot, endSlot];
    }
    return slots;
};

function encodeDay(day) {
    if (day == 1) return 'MO';
    else if (day== 2) return 'TU';
    else if (day == 3) return 'WE';
    else if (day == 4) return 'TH';
    else if (day == 5) return 'FR';
    else return '';
};

function encodeTime(col, span) {
    var startTime = Math.floor((col-1) / 2 + 8).toString();
    if (col % 2 != 0) startTime = startTime + ":00";
    else startTime = startTime + ":30";
    var endTime = Math.floor((col+span-1) / 2 + 8).toString();
    if ((col+span) % 2 != 0) endTime = endTime + ":00";
    else endTime = endTime + ":30";

    if (col == 19) {
        startTime = "18:00";
        endTime = "21:00";
    }


    return startTime+'-'+endTime;
};

// will be used in the update process
var itemList;

function printSchedule() {
    redips.clearTable();

    var type = document.getElementById("changeType").getAttribute("data-curType");
    var itemKey = document.getElementById("searchBox").value;
    var Theader = document.getElementById("tableHeader");
    if (type == 'Room') {
        Theader.innerHTML = "Room: ";
    } else {
        itemKey = document.getElementById("searchBox").value;
        Theader.innerHTML = "Instructor: ";
    }
    Theader.innerHTML += itemKey

    var mainTable =document.getElementById("mainTable"); 
    var notAssignedindex = 0;
    var errorIndex = 0;

    console.log('Load '+ type + ' ' + itemKey);

    itemList = {};
    if (type == 'Room') {
        for (var index in dataMap) {
          var entry = dataMap[index];
          if (entry['Room'] == itemKey) {
            itemList[entry['Row Index']] = entry;
          }
        }
    } else {
        for (var index in dataMap) {
          var entry = dataMap[index];
          if (entry['Instructor'].indexOf(itemKey) >= 0) {
            itemList[entry['Row Index']] = entry;
          }
        }
    }

    var timeSlot;
    for (var index in itemList) {
        var entry = itemList[index];
        var day = entry['Meeting Day'];

        var rowID = decodeDay(day);
        var colID = decodeTime(entry['Meeting Time']);
        var text;
        if (colID.length > 0) {
          timeSlot = mainTable.rows[rowID].cells[colID[0]];
          text = entry['Catalog Number'];
          if (entry['Section'].length > 0)
            text += '\n' + '(Section '+entry['Section']+')'
          if (type == 'Instructor')
            text += '\n' + entry['Room']

          redipsDrag.loadContent('mainTable', [[entry['Row Index'], rowID, colID[0], "blue", text]]);
        }
        if (colID.length > 1) {
            var startSlot = colID[0];
            var endSlot = colID[colID.length-1];

            for (var c = startSlot; c < endSlot; c++) {
                redipsTable.mark(true, mainTable, rowID, c);
            }
            redips.merge();
        }

        entry['Col Index'] = [rowID, colID[0]];
    }

    console.log('Load ' + type + ' ' + 'Done');
};

function createUpdateData(entries) {
  var values = [];

  for (var i=0; i < entries.length; i++) {
    var entry = entries[i];
    // var meetingDay = "";
    // for (var index = 0; index < entry['Meeting Day'].length; index++)
    //   meetingDay += " " + entry['Meeting Day'][index];
    // meetingDay = meetingDay.trim();

    var instructors = "";
    for (var index = 0; index < entry['Instructor'].length; index++)
      instructors += "," + entry['Instructor'][index];
    instructors = instructors.substring(1);

    var value = [Number(entry['Term']),
                  Number(entry['Catalog Number']),
                  entry['Course Title'],
                  Number(entry['Section']),
                  entry['Type'],
                  // meetingDay,
                  entry['Meeting Day'],
                  entry['Meeting Time'],
                  entry['Room'],
                  instructors,
                  entry['Remarks'],
                  entry['Midterm Exam Date'],
                  entry['Final Exam Date']
                ];
    values.push(value);
  }
  return values;
};

function createDeleteRequest(row) {
  // row starts from zero
  var request = {
    deleteDimension: {
      range: {
        sheetId: sheetID,
        dimension: "ROWS",
        startIndex: row-1,
        endIndex: row
      }
    }
  };
  return request;
};

function update(entry) {
    // insert new rows from the interface
    var data = createUpdateData([entry]);
    var rowID = entry['Row Index'];

    var dataBody = {
      range: "A"+rowID+":L"+rowID,
      values: data
    }

    var body = {
      data: dataBody,
      valueInputOption: "RAW"
    };
    console.log(body);

    gapi.client.sheets.spreadsheets.values.batchUpdate({
       spreadsheetId: SPREADSHEET_ID,
       resource: body
    }).then((response) => {
      var result = response.result;
      console.log(`${result.totalUpdatedCells} cells updated.`);
    });


}

function updateTable() {
  // get new data
  var newEntries = createNewEntries();
  // console.log(newEntries);

  // get old rows
  var updateSet = new Set();
  for (var index=0; index < newEntries.length; index++) {
    var entry = newEntries[index];
    var rowID = entry['Row Index'];
    updateSet.add(rowID);
  }
  let rowIDs = Array.from(updateSet);
  rowIDs.sort((a, b) => a - b);

  // delete old rows
  console.log(rowIDs);
  deleteRows(rowIDs);
  // reload data
  // loadData()

  // insert new entries
  insertRows(newEntries);

  // sort sheet
  sortSheet();
  // loadData();

  // refresh schedule
  printSchedule();
};

function sortSheet() {
  // sort Table
  var req = {
    sortRange: {
      range: {
        sheetId: sheetID,
        startRowIndex: 1,
      },
      sortSpecs: [
        {
          // sort by column A, semester
          dimensionIndex: 0,
          sortOrder: "ASCENDING"
        },
        {
          // sort by column B, catalog number
          dimensionIndex: 1,
          sortOrder: "ASCENDING"
        },
        {
          // sort by column D, section
          dimensionIndex: 3,
          sortOrder: "ASCENDING"
        },
        {
          // sort by column F, Meeting day
          dimensionIndex: 5,
          sortOrder: "ASCENDING"
        },
      ]
    }
  };

  var params = {
        spreadsheetId: SPREADSHEET_ID,
      };
  var batchUpdateSpreadsheetRequestBody = {
        requests: [req],
      };


  // call api
  var request = gapi.client.sheets.spreadsheets.batchUpdate(params, batchUpdateSpreadsheetRequestBody);
  request.then(function(response) {
    // on success
    console.log('Sheet Sorted');
  }, function(reason) {
    console.error('error: ' + reason.result.error.message);
  });
}

function deleteRows(rowIDs) {
  var requests = [];
  var nDeleted = 0;
  // rowID is the same as shown in sheet (starting from 1)
  for (var i=0; i < rowIDs.length; i++) {
    requests.push(createDeleteRequest(rowIDs[i]-nDeleted));
    nDeleted++;
  }

  var params = {
        spreadsheetId: SPREADSHEET_ID,
      };
  var batchUpdateSpreadsheetRequestBody = {
        requests: requests,
      };

  // call api
  var request = gapi.client.sheets.spreadsheets.batchUpdate(params, batchUpdateSpreadsheetRequestBody);
  request.then(function(response) {
    // on success
    console.log('Delete '+nDeleted+' rows');
    lastRow -= nDeleted;
  }, function(reason) {
    console.error('error: ' + reason.result.error.message);
  });
}

function insertRows(newEntries) {
  var requests = []
  // append empty rows for new data
  requests.push({
    appendDimension: {
      sheetId: sheetID,
      dimension: "ROWS",
      length: newEntries.length
    }
  });

  var params = {
        spreadsheetId: SPREADSHEET_ID,
      };
  var batchUpdateSpreadsheetRequestBody = {
        requests: requests,
      };

  // call api
  var request = gapi.client.sheets.spreadsheets.batchUpdate(params, batchUpdateSpreadsheetRequestBody);
  request.then(function(response) {
    // on success
    console.log('Append '+newEntries.length+' rows');
  }, function(reason) {
    console.error('error: ' + reason.result.error.message);
  });

    // insert new rows from the interface
    var data = createUpdateData(newEntries);

    var dataBody = {
      range: "A"+(lastRow+1).toString()+":L"+(lastRow+1+newEntries.length).toString(),
      values: data
    }

    var body = {
      data: dataBody,
      valueInputOption: "RAW"
    };
    // console.log(body);

    gapi.client.sheets.spreadsheets.values.batchUpdate({
       spreadsheetId: SPREADSHEET_ID,
       resource: body
    }).then((response) => {
      var result = response.result;
      console.log(`${result.totalUpdatedCells} cells updated.`);
    });

};

function createNewEntries() {
    var newEntries = [];
    var mainTable = document.getElementById('mainTable');
    var rowSize = mainTable.rows.length;
    for (var r=1; r < rowSize; r++) {
        var colSize = mainTable.rows[r].cells.length;
        for (var c = 1; c < colSize; c++) {
            var cell = mainTable.rows[r].cells[c];
            if (cell.childElementCount > 0) {
                var children = cell.children;
                for (var index=0; index < children.length; index++) {
                    var child = children[index];
                    // get rowID in Google Sheets, starting from 1
                    var rowID = child.id;

                    // get new position
                    var colID = cell.id.split('-');
                    var day = encodeDay(Number(colID[0]));
                    var time = encodeTime(Number(colID[1]), cell.colSpan);

                    console.log(rowID, day, time);

                    // rowID in dataMap starts from 0
                    // the first row in the sheet is the header
                    var entry = Object.assign({}, dataMap[rowID-2]);
                    entry['Meeting Time'] = time;
                    entry['Meeting Day'] = [day];
                    newEntries.push(entry);
                }
            }
        }
    }
    return newEntries;
};



//=============================================================================
// el is node from which will be found parent row
// div is reference of DIV element from whom is needed to read first number
function setMode(radioButton) {
	//REDIPS.drag.dropMode = radioButton.value;
	REDIPS.drag.dropMode = single;
};



//=============================================================================
 
// add onload event listener
if (window.addEventListener) {
	window.addEventListener('load', redips.init, false);
}
else if (window.attachEvent) {
	window.attachEvent('onload', redips.init);
}